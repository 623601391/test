/*#
 #
 ###################################################################
 #
 #	Function: 		The template of queue ADT, and concrete realization
 #	
 #	Author:	 		Deliang Yang
 #
 #	Version: 		1.0.0
 #
 #	Create Time: 	2014/8/21 9:20 Thursday
 #
 ###################################################################
 #
 #*/


#ifndef __QUEUE_H__
#define __QUEUE_H__

const int QUEUESIZE = 30;

template<class T>
class Queue
{
public:
	// constructor
	Queue(int size=QUEUESIZE);
	// destructor
	~Queue(void);
	// base function, insert the element at last 
	void Insert(const T& item);
	// delete the fist element, and return it
	T Delete(void);
	// some attribute method
	int isFull(void)const;
	int isEmpty(void)const;
	int length(void)const;
private:
	// state data
	T * data;
	// record the queue size
	int size;
	// record the current element postion
	int pos;
};

template<class T>
Queue<T>::Queue(int size):size(size), pos(0)
{
	if(size<=0)
	{
		std::cerr<<"Initial Error: size must bigger than zero"<<std::endl;
		exit(1);
	}
	data=new T[size];
}

template<class T>
void Queue<T>::Insert(const T& item)
{
	if(size==pos)
	{
		std::cerr<<"Queue(Insert): queue full"<<std::endl;
		return;
	}
	data[pos]=item;
	pos++;
}

template<class T>
T Queue<T>::Delete(void)
{
	if(pos==0)
	{
		std::cerr<<"Queue(Delete): queue empty"<<std::endl;
		exit(1);
	}
	T temp=data[0];
	int i=0;
	while(i<pos-1)
	{
		data[i]=data[i+1];
		i++;
	}
	pos--;
	return temp;
}

template<class T>
int Queue<T>::isFull(void)const
{
	return size==pos;
}

template<class T>
int Queue<T>::isEmpty(void)const
{
	return pos==0;
}

template<class T>
int Queue<T>::length(void)const
{
	return pos;
}


template<class T>
Queue<T>::~Queue(void)
{
	if(data!=NULL)
	{
		delete[] data;
		data=NULL;
	}
}

#endif

